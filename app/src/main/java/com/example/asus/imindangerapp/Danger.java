package com.example.asus.imindangerapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by asus on 19/08/2016.
 */
public class Danger extends Activity {
    TextView k;
    String num;
    String compare = "";


    String perm ="android.permission.CALL_PHONE";
    String nard = "android.permission.SEND_SMS";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.danger);
        k = (TextView)findViewById(R.id.textView2);

            promptSpeechInput();

        Intent fromMain = getIntent();
        num = fromMain.getStringExtra("number");


    }

    public void promptSpeechInput(){
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        i.putExtra(RecognizerIntent.EXTRA_PROMPT,"Say Something");


        try{
            startActivityForResult(i,100);

        }
        catch(ActivityNotFoundException e){
            Toast.makeText(Danger.this, "Sorry Device doesnt support speech language", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 1:if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                Intent call = new Intent(Intent.ACTION_CALL);
                call.setData(Uri.parse("tel: 911"));

                SmsManager sms = SmsManager.getDefault();
              //  Toast.makeText(Danger.this, "Damn", Toast.LENGTH_SHORT).show();
                sms.sendTextMessage(num,null,"I NEED HELP!",null,null);


            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 100){
            if(resultCode == RESULT_OK){
                ArrayList<String> resultd = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                compare = resultd.get(0);




                k.setText(resultd.get(0));
                if(resultd.get(0).equals("help me")) {
                    if (Integer.valueOf(android.os.Build.VERSION.SDK) < 23) {


                        SmsManager sms = SmsManager.getDefault();
                        sms.sendTextMessage(num,null,"I NEED HELP PLEASE",null,null);
                        Intent call = new Intent(Intent.ACTION_CALL);

                        call.setData(Uri.parse("tel: 911"));
                        startActivity(call);

                    } else {
                        Toast.makeText(Danger.this, "Damnsdsds", Toast.LENGTH_SHORT).show();
                        SmsManager sms = SmsManager.getDefault();


                        Intent call = new Intent(Intent.ACTION_CALL);

                        call.setData(Uri.parse("tel: 911"));


                        int josh = checkSelfPermission(nard);
                        int kobe = checkSelfPermission(perm);

                        if(kobe!= PackageManager.PERMISSION_GRANTED && josh !=PackageManager.PERMISSION_GRANTED){

                            requestPermissions(new String[]{perm,nard},1);
                           // requestPermissions(new String[]{nard},1);
                            return;
                        }
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{perm,nard},1);

                            return;

                        }

                        sms.sendTextMessage(num,null,"hehehe",null,null);
                        startActivity(call);

                    }
                }
                else{
                    promptSpeechInput();
                }
            }
        }
    }

    }

