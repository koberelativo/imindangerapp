package com.example.asus.imindangerapp;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ImageButton nextAct;
    ImageButton contact;
    EditText edit;
    String number;
    String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        nextAct = (ImageButton)findViewById(R.id.Danger);
        contact = (ImageButton)findViewById(R.id.setContact);
        edit = (EditText)findViewById(R.id.editText);
        contact.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);

                        startActivityForResult(intent,1);
                    }
                }
        );
        nextAct.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(number == null){
                            Toast.makeText(MainActivity.this, "Set A contact please", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Intent kobe = new Intent(MainActivity.this, Danger.class);
                            kobe.putExtra("number", number);

                            startActivity(kobe);

                        }
                    }
                }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null){
            Uri uri = data.getData();
            if(uri!=null){
                Cursor c = null;
                try{
                    c = getContentResolver().query(uri, new String[]{
                                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                                    ContactsContract.CommonDataKinds.Phone.TYPE,
                                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME},
                            null, null, null);
                    if (c != null && c.moveToFirst()) {
                         number = c.getString(0);
                        int type = c.getInt(1);
                        name = c.getString(2);
                        edit.setText(number+" - "+name);
                        //showSelectedNumber(type, number);
                    }
                }finally{
                    if (c != null) {
                        c.close();
                    }

                }

            }
        }
    }


}
